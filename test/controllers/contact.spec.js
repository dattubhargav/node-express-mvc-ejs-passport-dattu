const app = require('../../app.js')
const LOG = require('../../utils/logger.js')
const mocha = require('mocha')
const expect = require('chai').expect
var request = require('supertest')

LOG.debug('Starting test/controllers/contact.spec.js.')

mocha.describe('API Tests - Contact Controller', function () {
  mocha.describe('GET /contact', function () {
    mocha.it('responds with status 200', function (done) {
      request(app)
        .get('/contact')
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.status).to.be.equal(200)
          done()
        })
    })
  })
})
